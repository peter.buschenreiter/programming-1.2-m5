package grid;

import grid.model.Grid;
import grid.view.GridPresenter;
import grid.view.GridView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

// add the following line to Run Configurations as a VM option
// --module-path "/Users/peterbuschenreiter/Documents/KDG/Year1/Programming/Java Dependencies/javaFX/lib" --add-modules=javafx.controls,javafx.media

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		Grid model = new Grid(5,8);
		GridView view = new GridView();
		GridPresenter presenter = new GridPresenter(model, view);
		primaryStage.setScene(new Scene(view));
//		primaryStage.setResizable(false);
		presenter.addWindowEventHandlers();
		primaryStage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
