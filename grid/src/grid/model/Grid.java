package grid.model;

public class Grid {
	//private Attributes
	private int columns;
	private int rows;

	public Grid() {
		columns = 10;
		rows = 10;
	}

	public Grid(int columns, int rows) {
		this.columns = columns;
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public int getRows() {
		return rows;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}
}
