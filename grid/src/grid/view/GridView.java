package grid.view;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class GridView extends BorderPane {
	private Canvas canvas;
	private VBox controls;
	private Slider rowSlider;
	private Slider colSlider;

	public GridView() {
		initialiseNodes();
		layoutNodes();
	}

	private void initialiseNodes() {
		canvas = new Canvas();
		Label rowLabel = new Label("Rows");
		rowSlider = new Slider(2, 20, 10);
		Label colLabel = new Label("Columns");
		colSlider = new Slider(2, 20, 10);

		controls = new VBox(rowLabel, rowSlider, colLabel, colSlider);
	}

	private void layoutNodes() {
		canvas.setHeight(400);
		canvas.setWidth(640);
		setCenter(canvas);
		setBottom(controls);

		rowSlider.setShowTickMarks(true);
		rowSlider.setShowTickLabels(true);
		rowSlider.setMajorTickUnit(18);
		rowSlider.setMinorTickCount(17);
		rowSlider.setBlockIncrement(1);
		rowSlider.setSnapToTicks(true);

		colSlider.setShowTickMarks(true);
		colSlider.setShowTickLabels(true);
		colSlider.setMajorTickUnit(18);
		colSlider.setMinorTickCount(17);
		colSlider.setBlockIncrement(1);
		colSlider.setSnapToTicks(true);
	}

	void drawGrid(int columns, int rows) {
		GraphicsContext gc = canvas.getGraphicsContext2D();
		double height = canvas.getHeight();
		double width = canvas.getWidth();

		gc.setFill(Color.WHITE);
		gc.fillRect(0, 0, width, height);

		double colWidth = width / columns;
		double rowHeight = height / rows;

		for (int i = 1; i < columns; i++) {
			gc.strokeLine(colWidth * i, 0, colWidth * i, height);
		}

		for (int i = 1; i < rows; i++) {
			gc.strokeLine(0, rowHeight * i, width, rowHeight * i);
		}
	}

	Canvas getCanvas() {
		return canvas;
	}

	Slider getRowSlider() {
		return rowSlider;
	}

	Slider getColSlider() {
		return colSlider;
	}
}
