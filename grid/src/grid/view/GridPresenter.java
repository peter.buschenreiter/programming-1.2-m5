package grid.view;

import grid.model.Grid;
import javafx.stage.Window;

public class GridPresenter {
	private Grid model;
	private GridView view;

	public GridPresenter(Grid model, GridView view) {
		this.model = model;
		this.view = view;
		addEventHandlers();
		updateView();
	}

	private void addEventHandlers() {
		view.getRowSlider().setOnMouseDragged(mouseEvent -> {
			model.setRows((int) view.getRowSlider().getValue());
			updateView();
		});

		view.getColSlider().setOnMouseDragged(mouseEvent -> {
			model.setColumns((int) view.getColSlider().getValue());
			updateView();
		});
	}

	private void updateView() {
		view.getRowSlider().setValue(model.getRows());
		view.getColSlider().setValue(model.getColumns());

		view.drawGrid(model.getColumns(), model.getRows());
	}

	public void addWindowEventHandlers() {
		Window window = view.getScene().getWindow();

		//		ChangeListener<Number> stageHeightListener = (observable, oldValue, newValue) -> {
		//			view.getCanvas().setHeight((Double) newValue);
		//			updateView();
		//		};
		//
		//		ChangeListener<Number> stageWidthListener = (observable, oldValue, newValue) -> {
		//			view.getCanvas().setWidth((Double) newValue);
		//			updateView();
		//		};

		//		window.heightProperty().addListener(stageHeightListener);
		//		window.widthProperty().addListener(stageWidthListener);
	}
}
