package evaluations;

import evaluations.model.Evaluation;
import evaluations.model.Evaluations;
import evaluations.view.EvaluationsByCriteriaPresenter;
import evaluations.view.EvaluationsView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class EvaluationsByCriteriaMain extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		Evaluations model = new Evaluations();
		fillModel(model);
		EvaluationsView view = new EvaluationsView();
		new EvaluationsByCriteriaPresenter(model, view);
		primaryStage.setScene(new Scene(view));
		primaryStage.setTitle("Car Evaluations");
		primaryStage.setWidth(800.0);
		primaryStage.setHeight(600.0);
		primaryStage.show();
	}

	private void fillModel(Evaluations model) {
		model.add("Fiat", new Evaluation(3.0, 5.0, 5.0));
		model.add("Audi", new Evaluation(6.0, 10.0, 4.0));
		model.add("Ford", new Evaluation(2.0, 3.0, 6.0));

	}
}
