package evaluations.view;

import evaluations.model.Evaluation;
import evaluations.model.Evaluations;
import javafx.scene.chart.XYChart;

import java.util.Map;

public class EvaluationsByCriteriaPresenter {
	private static final String USER_RATING_LABEL = "User Rating";
	private static final String MILEAGE_LABEL = "Mileage";
	private static final String SAFETY_LABEL = "Safety";
	private final Evaluations model;
	private final EvaluationsView view;

	public EvaluationsByCriteriaPresenter(Evaluations model, EvaluationsView view) {
		this.model = model;
		this.view = view;

		this.addEventHandlers();
		this.updateView();
	}

	private void addEventHandlers() {
	}

	private void updateView() {
		view.getBarChart().getXAxis().setLabel("Criteria");

		for (Map.Entry<String, Evaluation> results : model.getResults().entrySet()) {
			String brand = results.getKey();
			Evaluation evaluation = results.getValue();

			XYChart.Series<String, Number> series = new XYChart.Series<>();
			series.setName(brand);
			series.getData().add(new XYChart.Data<>(USER_RATING_LABEL, evaluation.getUserRating()));
			series.getData().add(new XYChart.Data<>(MILEAGE_LABEL, evaluation.getMileage()));
			series.getData().add(new XYChart.Data<>(SAFETY_LABEL, evaluation.getSafety()));
			view.getBarChart().getData().add(series);
		}

		for (String brand : model.getResults().keySet()) {
			Evaluation eval = model.getResults().get(brand);
		}
	}
}
