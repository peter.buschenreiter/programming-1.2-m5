package evaluations.view;

import evaluations.model.Evaluation;
import evaluations.model.Evaluations;
import javafx.scene.chart.XYChart;

import java.util.Map;

public class EvaluationsByBrandPresenter {
	private static final String USER_RATING_LABEL = "User Rating";
	private static final String MILEAGE_LABEL = "Mileage";
	private static final String SAFETY_LABEL = "Safety";
	private final Evaluations model;
	private final EvaluationsView view;

	public EvaluationsByBrandPresenter(Evaluations model, EvaluationsView view) {
		this.model = model;
		this.view = view;

		this.addEventHandlers();
		this.updateView();
	}

	private void addEventHandlers() {
	}

	private void updateView() {
		view.getBarChart().getXAxis().setLabel("Brand");

		XYChart.Series<String, Number> ratings = new XYChart.Series<>();
		XYChart.Series<String, Number> milage = new XYChart.Series<>();
		XYChart.Series<String, Number> safety = new XYChart.Series<>();
		ratings.nameProperty().setValue(USER_RATING_LABEL);
		milage.nameProperty().setValue(MILEAGE_LABEL);
		safety.nameProperty().setValue(SAFETY_LABEL);

		for (Map.Entry<String, Evaluation> results : model.getResults().entrySet()) {
			String name = results.getKey();
			Evaluation evaluation = results.getValue();
			ratings.getData().add(new XYChart.Data<>(name, evaluation.getUserRating()));
			milage.getData().add(new XYChart.Data<>(name, evaluation.getMileage()));
			safety.getData().add(new XYChart.Data<>(name, evaluation.getSafety()));
		}

		view.getBarChart().getData().add(ratings);
		view.getBarChart().getData().add(milage);
		view.getBarChart().getData().add(safety);
	}
}
