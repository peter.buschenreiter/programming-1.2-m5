package evaluations.view;

import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.layout.BorderPane;


public class EvaluationsView extends BorderPane {
	BarChart<String, Number> barChart;

	public EvaluationsView() {
		this.initialiseNodes();
		this.layoutNodes();
	}

	private void initialiseNodes() {
		barChart = new BarChart<>(new CategoryAxis(), new NumberAxis());
	}

	private void layoutNodes() {
		setCenter(barChart);
	}

	BarChart<String, Number> getBarChart() {
		return barChart;
	}
}
