package pieChart.model;

import java.util.HashMap;

public class Survey {
	//private Attributes
	private final HashMap<String, Integer> data;

	public Survey() {
		data = new HashMap<>();
	}

	public void setEntry(String answer, int percentage) {
		data.put(answer, percentage);
	}

	public HashMap<String, Integer> getData() {
		return data;
	}
}
