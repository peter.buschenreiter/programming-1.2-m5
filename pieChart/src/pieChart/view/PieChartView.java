package pieChart.view;

import javafx.geometry.Insets;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class PieChartView extends BorderPane {
	private PieChart pie;
	private HBox hBox;
	private ComboBox<String> comboBox;
	private TextField textField;

	public PieChartView() {
		initialiseNodes();
		layoutNodes();
	}

	private void initialiseNodes() {
		pie = new PieChart();
		Label label = new Label("Change a slice");
		comboBox = new ComboBox<>();
		textField = new TextField();
		hBox = new HBox(label, comboBox, textField);
	}

	private void layoutNodes() {
		hBox.setPadding(new Insets(10));
		hBox.setSpacing(10);
		pie.setTitle("Where do I type semicolons?");
		setCenter(pie);
		setBottom(hBox);
	}

	PieChart getPie() {
		return pie;
	}

	ComboBox<String> getComboBox() {
		return comboBox;
	}

	TextField getTextField() {
		return textField;
	}
}
