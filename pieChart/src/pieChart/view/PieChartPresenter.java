package pieChart.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import pieChart.model.Survey;

public class PieChartPresenter {
	private final Survey model;
	private final PieChartView view;

	public PieChartPresenter(Survey model, PieChartView view) {
		this.model = model;
		this.view = view;

		addEventHandlers();
		updateView();
	}

	private void addEventHandlers() {
		view.getTextField().setOnAction(actionEvent -> {
			setValuesToPie();
			updateView();
		});

		view.getComboBox().setOnAction(actionEvent -> {
			view.getTextField().setText(String.valueOf(model.getData().get(view.getComboBox().getValue())));
		});
	}

	private void updateView() {
		String chosen = view.getComboBox().getValue();
		ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
		model.getData().forEach((answer, percentage) -> pieChartData.add(new PieChart.Data(answer, percentage)));
		view.getPie().setData(pieChartData);

		view.getComboBox().getItems().clear();
		view.getComboBox().getItems().addAll(model.getData().keySet());

		if (chosen == null) {
			view.getComboBox().getSelectionModel().select(0);

		} else {
			view.getComboBox().setValue(chosen);
		}
	}

	private void setValuesToPie() {
		String answer = view.getComboBox().getValue();
		int percentage = Integer.parseInt(view.getTextField().getText());
		model.setEntry(answer, percentage);
	}
}
