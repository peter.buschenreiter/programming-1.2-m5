package pieChart;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pieChart.model.Survey;
import pieChart.view.PieChartPresenter;
import pieChart.view.PieChartView;

// add the following line to Run Configurations as a VM option
// --module-path "/Users/peterbuschenreiter/Documents/KDG/Year1/Programming/Java Dependencies/javaFX/lib" --add-modules=javafx.controls,javafx.media


public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		Survey model = new Survey();
		model.setEntry("Text", 1);
		model.setEntry("Code", 19);
		model.setEntry(";)", 80);

		PieChartView view = new PieChartView();
		new PieChartPresenter(model, view);
		primaryStage.setScene(new Scene(view));
		primaryStage.show();
	}
}
