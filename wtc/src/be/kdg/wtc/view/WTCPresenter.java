package be.kdg.wtc.view;

public class WTCPresenter {
	private final WTCView view;

	public WTCPresenter(WTCView view) {
		this.view = view;
		addEventHandlers();
		updateView();
		view.getCanvas().getGraphicsContext2D().setImageSmoothing(true);
	}

	private void addEventHandlers() {
		view.getSlider().setOnMouseDragged(event -> updateView());
	}

	private void updateView() {
		view.updateCanvas(view.getSlider().getValue());
	}
}
