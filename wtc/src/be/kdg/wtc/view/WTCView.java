package be.kdg.wtc.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;

public class WTCView extends VBox {
	private static final Image BEFORE = new Image("/images/before.jpg");
	private static final Image AFTER = new Image("/images/after.jpg");

	private static final double IMAGE_WIDTH = 900;
	private static final double IMAGE_HEIGHT = 595;

	private Canvas canvas;
	private Slider slider;

	public WTCView() {
		initialiseNodes();
		layoutNodes();
	}

	private void initialiseNodes() {
		canvas = new Canvas();
		slider = new Slider(0, 100, 50);
	}

	private void layoutNodes() {
		canvas.setWidth(IMAGE_WIDTH);
		canvas.setHeight(IMAGE_HEIGHT);
		slider.setMinWidth(IMAGE_WIDTH + 14);

		setSpacing(5);
		setPadding(new Insets(5));
		setAlignment(Pos.CENTER);

		getChildren().addAll(canvas, slider);
	}

	Slider getSlider() {
		return slider;
	}

	Canvas getCanvas() {
		return canvas;
	}

	/**
	 @param percentage A value from 0 to 100. The percentage of the "before" picture that should be displayed.
	 */
	void updateCanvas(double percentage) {
		double sliderWidth = canvas.getWidth() * percentage / 100;

		GraphicsContext gc = canvas.getGraphicsContext2D();
		gc.drawImage(BEFORE, 0, 0, sliderWidth, canvas.getHeight(), 0, 0, sliderWidth, canvas.getHeight());
		gc.drawImage(AFTER, sliderWidth, 0, canvas.getWidth(), canvas.getHeight(), sliderWidth, 0, canvas.getWidth(), canvas.getHeight());

		gc.setLineWidth(3);
		gc.strokeLine(sliderWidth, 0, sliderWidth, canvas.getHeight());
	}
}
